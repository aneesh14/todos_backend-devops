# upload pip package
echo """
[distutils]
index-servers = local
[local]
repository: https://${JFROG_HOST}.jfrog.io/${JFROG_HOST}/api/pypi/pypi-local
username: ${JFROG_USERNAME}
password: ${JFROG_PASSWORD}
""" > ~/.pypirc

# download pip package
mkdir -p ~/.pip
echo """
[global]
index-url = https://${JFROG_USERNAME}:${JFROG_PASSWORD}@${JFROG_HOST}.jfrog.io/${JFROG_HOST}/api/pypi/pypi-local/simple
extra-index-url = https://pypi.python.org/simple
""" > ~/.pip/pip.conf