from django.db import models


# Create your models here.


class Todo(models.Model):
    description = models.CharField(max_length=200)
    is_completed = models.BooleanField()
    remarks = models.TextField(default="")

    @classmethod
    def create_todo(cls, description):
        return cls.objects.create(
            description=description,
            is_completed=False,
            remarks=""
        )

    def complete_todo(self):
        self.is_completed = True
        self.save()
