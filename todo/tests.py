from django.test import TestCase


# Create your tests here.

class TestTodo(TestCase):

    def test_create_todo(self):
        from todo.models import Todo
        self.assertEqual(Todo.objects.all().count(), 0)
        todo_obj = Todo.create_todo("my first todo")
        self.assertEqual(todo_obj.id, 1)
        self.assertEqual(Todo.objects.all().count(), 1)

    def test_complete_todo(self):
        from todo.models import Todo
        todo_obj = Todo.create_todo("my first todo")
        self.assertEqual(todo_obj.is_completed, False)
        todo_obj.complete_todo()
        self.assertEqual(todo_obj.is_completed, True)
